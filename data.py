from files_utils import *
from PIL import Image as PILImage
import numpy
from PIL.ImageQt import ImageQt

def load_files_to_array(folder_path):
	all_files = get_files(folder_path)
	imlist=[filename for filename in all_files if filename[-4:] in [".png",".PNG", ".bmp"]]
	if not imlist:
		return
	print("== Processing {} image(s) in folder {}".format(len(imlist), folder_path))

	# Assuming all images are the same size, get dimensions of first image
	w,h=PILImage.open(imlist[0]).convert('L').size
	N=len(imlist)

	

	array_path_list = []
	for img_path in imlist:
		
		img = PILImage.open(img_path)
		img = img.convert('L')
		if (w, h) == (160, 144):
			#remove frame
			img = img.crop( (16, 16, 16+128, 16+112) )		
		
		arr = numpy.array(img,dtype=float) / 255
		array_path_list.append( (arr, img_path) )
		
	return array_path_list

def array_to_ImageQt(arr):
	formatted = (255*arr).astype('uint8')
	imgPIL = PILImage.fromarray(formatted)
	return ImageQt(imgPIL)

