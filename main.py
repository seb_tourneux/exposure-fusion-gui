import sys
from PySide6.QtCore import QPointF, Qt
from PySide6.QtWidgets import QMainWindow, QApplication, QScrollArea, QVBoxLayout, QHBoxLayout, QLabel, QWidget, QSlider
from PySide6.QtCharts import QChart, QChartView, QLineSeries, QAreaSeries
from PySide6.QtGui import QGradient, QPen, QLinearGradient, QPainter, QPalette, QImage, QPixmap


import data
import process



class MainWindow(QMainWindow):
	def __init__(self):
		super(MainWindow, self).__init__()

		# todo export settings
		self.lum_value = 0.0
		
		self.setWindowTitle("exposure-fusion-gui")
		#self.setBackgroundRole(QPalette.Dark)
		
		self.main_layout = QHBoxLayout()
		self.main_widget = QWidget()
		self.main_widget.setLayout(self.main_layout)
		
		# input image
		self.input_scroll_area = QScrollArea()	
		self.main_layout.addWidget(self.input_scroll_area)
		
		# result
		self.right_layout = QVBoxLayout()
		self.right_widget = QWidget()
		self.right_widget.setLayout(self.right_layout)
		self.image_res_widget = QWidget()
		
		self.methods = ["average", "mertens"]
		self.result_dict = {}
		res_images_layout = QVBoxLayout()

		for method in self.methods:
			img_label_layout = QVBoxLayout()

			print("label method {}".format(method))
			label_method = QLabel()
			label_method.setText(method)
			label_method.setAttribute(Qt.WA_StyledBackground, True)
			label_method.setStyleSheet('background-color: red;')

			img_label_layout.addWidget(label_method)

			image_label = QLabel()
			image_label.setToolTip(method)
			self.result_dict[method] = image_label
			
			img_label_layout.addWidget(image_label)
			

			
			img_label_widget = QWidget()
			img_label_widget.setLayout(img_label_layout)
			
			res_images_layout.addWidget(img_label_widget)


		self.image_res_widget.setLayout(res_images_layout)
		self.right_layout.addWidget(self.image_res_widget)
		
		self.lum_slider = QSlider(Qt.Horizontal, self)
		#self.lum_slider.setGeometry(10, 10, 300, 40)
		self.lum_slider.valueChanged.connect(self.lum_slider_value_changed)
		self.right_layout.addWidget(self.lum_slider)

		self.main_layout.addWidget(self.right_widget)
		self.setCentralWidget(self.main_widget)
	
	def lum_slider_value_changed(self, value):
		self.lum_value = value / 10.0
		self.update()
		
	def update(self):
		input_images_layout = QVBoxLayout()
		folder_path = "input\\olympus1a"
		
		# todo : store that (ie do not reload from disk every update)
		array_paths = data.load_files_to_array(folder_path)
		for (arr, path) in array_paths:
			image_label = QLabel()
			pixmap = QPixmap(data.array_to_ImageQt(arr))
			image_label.setPixmap(pixmap)
			image_label.setToolTip(path)
			
			input_images_layout.addWidget(image_label)
		
		input_images_widget = QWidget()
		input_images_widget.setLayout(input_images_layout)
		self.input_scroll_area.setWidget(input_images_widget)



		print("Updating with lum {}".format(self.lum_value))
		
		arrays = list(zip(*array_paths))[0]
		average_arr = process.average(arrays)
		#average_arr = process.gammaCorrection(average_arr, self.lum_value)
		
		ratio = 3.0
		average_pixmap = QPixmap(data.array_to_ImageQt(average_arr)).scaledToWidth(ratio*average_arr.shape[1])
		self.result_dict["average"].setPixmap(average_pixmap)

		mertens_arr = process.merge_mertens(arrays, self.lum_value)
		mertens_pixmap = QPixmap(data.array_to_ImageQt(mertens_arr)).scaledToWidth(ratio*mertens_arr.shape[1])
		self.result_dict["mertens"].setPixmap(mertens_pixmap)


			


		

if __name__ == "__main__":
	if not QApplication.instance():
		app = QApplication(sys.argv)
	else:
		app = QApplication.instance()

	window = MainWindow()
	window.update()
	window.show()
	window.resize(500, 500)
	sys.exit(app.exec())