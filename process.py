#from typing import TYPE_CHECKING, Any
import numpy as np
import cv2 as cv



def average(arrays):
	arr=np.zeros(arrays[0].shape,float)
	N = len(arrays)
	for imarr in arrays:
		arr= arr + imarr/N
	return arr


def gammaCorrection(arr, gamma):
	invGamma = 1 / max(0.01, gamma)

	return arr ** invGamma


def merge_mertens(arrays, exposure):
	
	#return average(arrays)

	constrast = 0.5
	#TODO
	print("Mertens : Wcontrast{} Wexposure {}".format(constrast, exposure))
	merge_mertens = cv.createMergeMertens(constrast, 0.0, exposure)
	res_mertens = merge_mertens.process(arrays)
	
	res_mertens = np.interp(res_mertens, (res_mertens.min(), res_mertens.max()), (0, 1))
	return res_mertens